SELECT 'Edificio' AS [Nombre], [edif].[edif_descripcion] + ' - Codigo cliente:' + [edif].[edif_codigo_corto] AS [Dato], [f].[IdFactura]
FROM     [dbo].[ao_ventas_cobranza_fac] AS [f] INNER JOIN
                  [dbo].[ao_ventas_cabecera] AS [c] ON [f].[venta_codigo] = [c].[venta_codigo] INNER JOIN
                  [dbo].[gc_edificaciones] AS [edif] ON [c].[edif_codigo] = [edif].[edif_codigo]
UNION
SELECT 'Equipo' AS [Nombre], [d].[concepto_venta] AS [Dato], [f].[IdFactura]
FROM     [dbo].[ao_ventas_cobranza_fac] AS [f] INNER JOIN
                  [dbo].[ao_ventas_detalle] AS [d] ON [f].[venta_codigo] = [d].[venta_codigo]