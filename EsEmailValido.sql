USE [ADMIN_EMPRESA]
GO
/****** Object:  UserDefinedFunction [dbo].[EsEmailValido]    Script Date: 16/9/2022 12:15:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		devcastro9
-- Create date: 7/9/2022
-- Description:	Validador temporal de email
-- =============================================
ALTER FUNCTION [dbo].[EsEmailValido]
(
	@email VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declara la variable de retorno
	DECLARE @email_valido VARCHAR(50)

	-- T-SQL
	IF @email IS NULL OR LEN(@email) < 300
	BEGIN
		SET @email_valido = 'otisbolivia@gmail.com'
	END
	ELSE
	BEGIN
		SET @email_valido = @email
	END

	-- Retorna el resultado de la funcion
	RETURN @email_valido
END
