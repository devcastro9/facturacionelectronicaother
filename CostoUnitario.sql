USE [ADMIN_EMPRESA]
GO
/****** Object:  UserDefinedFunction [dbo].[CostoUnitario]    Script Date: 16/9/2022 12:11:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		devcastro9
-- Create date: 7/9/2022
-- Description:	Validador temporal de email
-- =============================================
ALTER FUNCTION [dbo].[CostoUnitario]
(
	@tipo_moneda VARCHAR(4),
	@bob DECIMAL(18,2),
	@usd DECIMAL(18,2)
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @valor DECIMAL(18,5) = 0

	-- Add the T-SQL statements to compute the return value here
	IF @tipo_moneda = 'BOB'
	BEGIN
		SET @valor = @bob
	END
	ELSE
	BEGIN
		SET @valor = @usd
	END
	-- Return the result of the function
	RETURN @valor
END
