USE [ADMIN_EMPRESA]
GO
/****** Object:  StoredProcedure [dbo].[facActualizacionEstados]    Script Date: 16/9/2022 12:16:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		devcastro9
-- Create date: 11/09/2022
-- Modified date: 15/09/2022
-- Description:	Procedimiento almacenado que actualiza los valores necesario para la facturacion electronica
-- =============================================
ALTER PROCEDURE [dbo].[facActualizacionEstados]
AS
BEGIN
	-- SET NOCOUNT ON
	SET NOCOUNT ON;

    -- T-SQL
	/*
	------------------------------------------------------------------------------
		Queries para actualizar el EstadoFacId que lee el proceso en segundo plano
	------------------------------------------------------------------------------
	*/
	-- Factura nueva en cola de emision pendiente (No se emite)
	UPDATE [dbo].[ao_ventas_cobranza_fac] SET [EstadoFacId] = 0 WHERE [estado_codigo_fac] = 'REG' OR [EstadoFacId] IS NULL
	-- Factura nueva en cola de emision (Para envio)
	UPDATE [dbo].[ao_ventas_cobranza_fac] SET [EstadoFacId] = 1 WHERE [estado_codigo_fac] = 'APR' AND [EstadoFacId] = 0
	-- Factura en cola de anulacion (Para envio)
	UPDATE [dbo].[ao_ventas_cobranza_fac] SET [EstadoFacId] = 5 WHERE [estado_codigo_fac] = 'ANL' AND [EstadoFacId] = 9
	/*
	----------------------------------------------------------------------
		Metodos de completado para el codigo de empresa (1 = CGI, 2 = CGE)
	-----------------------------------------------------------------------
	*/
	UPDATE [f] SET [f].[codigo_empresa] = [c].[codigo_empresa] FROM [dbo].[ao_ventas_cobranza_fac] AS [f] INNER JOIN [dbo].[ao_ventas_cabecera] AS [c] ON [f].[venta_codigo] = [c].[venta_codigo] WHERE [f].[codigo_empresa] IS NULL OR [f].[codigo_empresa] = 0
	/*
	-------------------------------------------------------------------------------------------
		Metodos de completado del tipo de documento (1 = CI, 2 = CEX, 3 = PSP, 4 = OD, 5 = NIT)
	-------------------------------------------------------------------------------------------
	*/
	-- Total
	-- UPDATE [f] SET [f].[tipodoc_codigo] = [b].[tipodoc_codigo] FROM [dbo].[ao_ventas_cobranza_fac] AS [f] INNER JOIN [dbo].[gc_beneficiario] AS [b] ON [f].[beneficiario_codigo_fac] = [b].[beneficiario_codigo]
	-- Solo los necesarios
	UPDATE [f] SET [f].[tipodoc_codigo] = [b].[tipodoc_codigo] FROM [dbo].[ao_ventas_cobranza_fac] AS [f] INNER JOIN [dbo].[gc_beneficiario] AS [b] ON [f].[beneficiario_codigo_fac] = [b].[beneficiario_codigo] WHERE [f].[tipodoc_codigo] IS NULL
	/*
	-------------------------------------------------------------------------------------------
		Metodo de completado de departamento (depto_codigo)
	-------------------------------------------------------------------------------------------
	*/
	UPDATE [f] SET [f].[depto_codigo] = [c].[depto_codigo] FROM [dbo].[ao_ventas_cobranza_fac] AS [f] INNER JOIN [dbo].[ao_ventas_cabecera] AS [c] ON [f].[venta_codigo] = [c].[venta_codigo] WHERE [f].[depto_codigo] IS NULL
	/*
	-------------------------------------------------
		Facturas ya emitidas en Libelula
	-------------------------------------------------
	*/
	--UPDATE [dbo].[ao_ventas_cobranza_fac] SET [EstadoFacId] = 9 WHERE [IdFactura] IN (83378, 83388, 83379, 83384, 83348, 83392, 83386, 83355, 83397, 83339)
END
