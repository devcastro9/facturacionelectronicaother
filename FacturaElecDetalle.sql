SELECT fac.IdFactura, p.CodActividad AS actividad, p.CodProducto AS producto_sin, t.CodigoProducto AS producto, d.cobranza_observaciones AS concepto, 1 AS Cantidad, 58 AS UnidadMedida, dbo.CostoUnitario(fac.tipo_moneda, 
                  d.cobranza_total_bs, d.cobranza_total_dol) AS costo
FROM     dbo.ao_ventas_cobranza_fac AS fac INNER JOIN
                  dbo.ao_ventas_cobranza AS d ON fac.IdFactura = d.venta_codigo_new INNER JOIN
                  dbo.ao_ventas_cabecera AS c ON d.venta_codigo = c.venta_codigo INNER JOIN
                  dbo.gc_tipo_transaccion AS t ON c.trans_codigo = t.trans_codigo INNER JOIN
                  dbo.gc_productos_sin AS p ON t.CorrelativoSIN = p.correlativo