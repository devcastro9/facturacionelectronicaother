SELECT c.AppKey, f.IdFactura, dbo.CostoUnitario(f.tipo_moneda, f.total_bs, f.total_dol) AS monto, f.JustificaAnulacionFac AS motivo
FROM     dbo.ao_ventas_cobranza_fac AS f INNER JOIN
                  dbo.facConfiguracion AS c ON f.codigo_empresa = c.EmpresaId INNER JOIN
                  dbo.facEstado AS e ON f.EstadoFacId = e.EstadoFacId INNER JOIN
                  dbo.facEstadoTipo AS t ON e.TipoFacId = t.TipoFacId
WHERE  (c.HabilitadoFacturacion = 1) AND (c.FacturacionBackgroundActivo = 1) AND (c.Estado = 1) AND (e.Habilitado = 1) AND (t.EsCola = 1)