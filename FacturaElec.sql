SELECT conf.AppKey, c.IdFactura, dbo.EsEmailValido(c.beneficiario_email) AS email, conf.CallbackUrlBase AS curl, c.beneficiario_RazonSocial AS razon_social, c.beneficiario_nit AS nit, doc.DocIdentidadSIN AS tipo_doc, 
                  c.edif_codigo_corto AS codigo_cliente, c.glosa_Descripcion AS factura_nota, '0' AS tipo_factura, 1 AS emite_factura, c.cambio_oficial, c.tipo_moneda, conf.CanalCaja AS canal_caja, c.depto_codigo AS canal_caja_sucursal, 
                  dep.depto_descripcion AS canal_caja_usuario, c.estado_codigo_fac, conf.ValidarEmail, conf.ValidarNIT
FROM     dbo.ao_ventas_cobranza_fac AS c INNER JOIN
                  dbo.facConfiguracion AS conf ON c.codigo_empresa = conf.EmpresaId INNER JOIN
                  dbo.gc_tipo_documento_id AS doc ON c.tipodoc_codigo = doc.tipodoc_codigo INNER JOIN
                  dbo.facEstado AS e ON c.EstadoFacId = e.EstadoFacId INNER JOIN
                  dbo.facEstadoTipo AS t ON e.TipoFacId = t.TipoFacId INNER JOIN
                  dbo.gc_departamento AS dep ON c.depto_codigo = dep.depto_codigo
WHERE  (t.EsCola = 1) AND (e.Habilitado = 1) AND (conf.HabilitadoFacturacion = 1) AND (conf.FacturacionBackgroundActivo = 1) AND (conf.Estado = 1) AND (t.TipoFacId = 0)